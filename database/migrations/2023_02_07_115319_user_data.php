<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_data', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('email');
            $table->string('company');
            $table->string('phone');
            $table->integer('annual_kw_hr')->default(0);
            $table->double('electricity_rate')->default(0);
            $table->double('savings_due_to_Own_Consumption')->default(0);
            $table->double('Export')->default(0);
            $table->double('Savings_due_to_export')->default(0);
            $table->double('Total_Annual_Savings')->default(0);
            $table->double('Payback_Period')->default(0);
            $table->double('Estimated_Annual_Elec_Bill')->default(0);
            $table->double('Energy_Bill_Savings_Annually')->default(0);
            $table->double('Annual_Carbon_Savings')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_data');
    }
};
