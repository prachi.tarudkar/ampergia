<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('solar_calculator_data', function (Blueprint $table) {
            $table->id();
            $table->integer('annual_kw_hr')->default(0);
            $table->double('solar_array')->default(0);
            $table->integer('cost')->default(0);
            $table->double('annaul_generation_kw')->default(0);
            $table->double('own_comsumption')->default(0);  
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('solar_calculator_data');
    }
};
