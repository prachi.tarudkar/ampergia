<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\SolarCalculatorController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::controller(SolarCalculatorController::class)->group(function () {
Route::get('/calculatesolar', 'calculateSolar');
Route::post('/storeSolarCalculation', 'storeSolarCalculation');
Route::post('/storeCalculation', 'storeCalculation');
});

/*Route::get('/calculatesolar', [SolarCalculatorController::class, 'calculateSolar']);*/
// Route::get('/calculatesolar', 'SolarCalculatorController@calculateSolar');
//Route::post('/storeSolarCalculation','SolarCalculatorController@storeSolarCalculation');