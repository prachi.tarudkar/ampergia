<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ElectricityRate extends Model
{
    
    protected $fillable = ['rate'];

    
}