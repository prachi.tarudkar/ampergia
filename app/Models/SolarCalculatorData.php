<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SolarCalculatorData extends Model
{
    
    protected $fillable = ['annual_kw_hr','solar_array','cost'];

    /*public function Solarcalculatordata()
    {
       return $this->belongsTo('App\SolarCalculatorData', 'id');
    }
*/
}