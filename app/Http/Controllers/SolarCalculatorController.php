<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use App\Models\SolarCalculatorData;
use App\Models\ElectricityRate;
//use Illuminate\View\Middleware\ShareErrorsFromSession;
use DB;

class SolarCalculatorController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('guest');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

    public function calculateSolar(Request $request){
       // $data = $request->all();
       /*$data =  DB::table("solar_calculator_data")->get();
       dd($data);*/
        $annual_kw_hr  = SolarCalculatorData::pluck('annual_kw_hr');
       // dd($annual_kw_hr);

       /*$annual_kw_hr= array(0 => "1000",
    1 => "2000",
    2 => "3000",
    3 => "4000",
    4 => "5000",
    5 => "6000",
    6 => "7000",
    7 => "8000",
    8 => "9000",
    9 => "10000",
    10 => "11000",
    11 => "12000",
    12 => "13000",
    14 => "14000",
    15 => "15000",
    16 => "16000",
    17 => "17000",
    18 => "18000",
    19 => "19000",
    20 => "20000");*/
    $electricity_rate = ElectricityRate::pluck('rate');
       /*$electricity_rate = array(0 => "1000",
    1 => "0.1",
    2 => "0.11",
    3 => "0.12",
    4 => "0.13",
    5 => "0.14",
    6 => "0.15",
    7 => "0.16",
    8 => "0.17",
    9 => "0.18",
    10 => "0.19",
    11 => "0.2",
    12 => "0.21",
    14 => "0.22",
    15 => "0.23",
    16 => "0.24",
    17 => "0.25",
    18 => "0.26",
    19 => "0.27",
    20 => "0.28");*/
    
  //dd($annual_kw_hr);
        /*$responsedata = array('email' => $data['email'],

                          );*/

    //return view('solarcalcutator',compact(array('annual_kw_hr','responsedata')));
    return view('solarcalcutator',compact(array('annual_kw_hr','electricity_rate')));
    }

    public function storeSolarCalculation(Request $request){
        //dd($request->all());
        $esti_annual_ele = $request['annual_kw_hr'];
        dd($esti_annual_ele);

    }
public function storeCalculation(Request $request){
//$data = $request->all();
//dd($request->all());
    /*$request->validate([
        'name' => 'required',
        'email' => 'required',
    ]);*/
    $data = DB::table("solar_calculator_data")->where('annual_kw_hr',$request['annual_kw_hr'])->first();
    //dd($data);
    //name ,email,company,tel
    $esti_annual_ele = $request['annual_kw_hr'];
    $ele_rate = $request['electricity_rate'];
    $Savings_due_to_Own_Consumption = $ele_rate * $data->own_comsumption;
    $cost = $data->cost;
    $Export = 0.4 * $data->annaul_generation_kw;
    $Savings_due_to_export = 0.07 * $Export;
    $Total_Annual_Savings = $Savings_due_to_Own_Consumption + $Savings_due_to_export;
    $Payback_Period = $data->cost/($Total_Annual_Savings*0.95);
    $Estimated_Annual_Elec_Bill = $esti_annual_ele*$ele_rate+(365*0.3);
    $Energy_Bill_Savings_Annually  = $Total_Annual_Savings/$Estimated_Annual_Elec_Bill;
    $Annual_Carbon_Savings = 0.193*$data->annaul_generation_kw;
    $solar_array = $data->solar_array;
    /*if($validate->fails()){
      return back()->withErrors($validate->errors())->withInput();
    }*/
       // dd($esti_annual_ele);
        /*dd($request->all());
*/

        // name,email,company,telephone,annual_kw_hr,electricity_rate,savings_due_to_Own_Consumption,Export,Savings_due_to_export,Total_Annual_Savings,Payback_Period,Estimated_Annual_Elec_Bill,Energy_Bill_Savings_Annually,Annual_Carbon_Savings;
        $save_data = array(
            "name"=>$request['name'],
            'email' => $request['email'],
            'company' => $request['company'],
            'phone' => $request['tel'],
            'annual_kw_hr' => $request['annual_kw_hr'],
            'electricity_rate' => $ele_rate,
            'savings_due_to_Own_Consumption' => round($Savings_due_to_Own_Consumption, 2),
            'Export' => round($Export, 2),
            'Savings_due_to_export' => round($Savings_due_to_export, 2),
            'Total_Annual_Savings' => round($Total_Annual_Savings, 2),
            'Payback_Period' => round($Payback_Period, 2),
            'Estimated_Annual_Elec_Bill' => round($Estimated_Annual_Elec_Bill, 2),
            'Energy_Bill_Savings_Annually' => round($Energy_Bill_Savings_Annually, 2),
            'Annual_Carbon_Savings' => round($Annual_Carbon_Savings, 2),
            'cost' => $cost,
            'solar_array' => $solar_array

        );

        DB::table('user_data')->insert($save_data);
        // $save_data = array('name'=>$request['annual_kw_hr']);
        return response(json_encode($save_data));
        //echo 1 ;exit;

    }
}

//}
