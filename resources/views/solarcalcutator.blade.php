<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8">
    <title>Solar Calculator</title>
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@200;300;400;500&display=swap" rel="stylesheet">
    <!-- <script src="https://cdn.jsdelivr.net/jquery/1.12.4/jquery.min.js"></script>
    <script src="https://cdn.jsdelivr.net/jquery.validation/1.15.1/jquery.validate.min.js"></script> -->
    <link href="css/style.css" rel="stylesheet" />
    <link href="css/calculator.css" rel="stylesheet" />
    <meta name="csrf-token" content="{{ csrf_token() }}" />
  </head>
  <body>
    <div class="solar-calculator">
        <header>
            <div class="container">
                <h1>Solar Calculator</h1>
            </div>
        </header>
        <!-- <main> -->
            <!-- <div class="container"> -->
                <!-- <form action="{{ url('storeCalculation') }}" method="post" name="solarcalculatorform" id="solar_calculatorform"> -->
                <form action=""  name="solarcalculatorform" id="solar_calculatorform">
                 <div class="container">
                <h3>To estimate your solar system, please provide the following details.</h3>   
               <!--  {{csrf_field()}} -->
                    <div class="card">
                        <h2>Enter Personal Details</h2>

                        <div class="grid-container">
                            <div class="grid-item">
                                <!-- <label for="name">Name</label> -->
                                <!-- <input type="text" id="name" name="name" class="@error('name') is-invalid @enderror form-control" placeholder="Name" required> -->
                               <!--  @error('name')
                                  <div class="alert alert-danger mt-1 mb-1">{{ $message }}</div>
                                @enderror -->

                                <input type="text" id="name" name="name" class="form-control" placeholder="Name" required>
                            </div>
                            <div class="grid-item">
                                <!-- <label for="email">Email Address</label> -->
                                <input type="email" id="email" name="email" class="form-control" placeholder="Email Address" required>
                            </div>
                      
                            <div class="grid-item">
                                <!-- <label for="company">Company Name</label> -->
                                <input type="text" id="company" name="company" class="form-control" placeholder="Company Name" required>
                            </div>
                            <div class="grid-item">
                                <!-- <label for="tel">Telephone</label> -->
                                <input type="tel" id="tel" name="tel" class="form-control" placeholder="Telephone" required>
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <h2>Select Parameters</h2>
                        <div class="grid-container">
                            <div class="grid-item">
                                <div class="datalist-container">
                                  <div class="select">  
                                    <select class="form-control" id="annual_kw_hr" name="annual_kw_hr">
                                        <option value="" disabled selected></option>
                                        @foreach($annual_kw_hr as $get_kw_hr)
                                          <option value="{{$get_kw_hr}}"> ${{$get_kw_hr}} </option>
                                        @endforeach
                                    </select>
                                    <label class="select-label">Estimated Annual kW Hours Usage </label>
                                </div>
                                </div>
                            </div>
                            <div class="grid-item">
                                <div class="datalist-container">
                                    <div class="select">
                                    <select class="form-control" name="electricity_rate" id="electricity_rate">
                                        <option value="" disabled selected></option>
                                        @foreach($electricity_rate as $electricity_rates)
                                            <option value="{{$electricity_rates}}"> {{$electricity_rates}} </option>
                                        @endforeach

                                        <!-- <option value="" disabled selected hidden>Electric Tarrif(&pound;)</option>electricity_rate
                                        <option value="1000">1000</option>
                                        <option value="2000">2000</option>
                                        <option value="3000">3000</option>
                                        <option value="4000">4000</option>
                                        <option value="5000">5000</option>
                                        <option value="6000">6000</option>
                                        <option value="7000">7000</option>
                                        <option value="8000">8000</option>
                                        <option value="9000">9000</option> -->
                                    </select>
                                    <label class="select-label">Electric Tarrif(&pound;)</label>
                                </div>
                                </div>
                            </div>
                        </div>
                    </div>
                     <!-- <input type="submit" name="submit" class="form-control btn btn-success">    -->
                     <div class="calculate-button">
                     <button type="submit" name="submit" class="btn">Calculate</button>
                 </div>
             </div>
                </form>
            </div>

            

            <div class="estimation" id="result_table" style="display:none">
                <div class="estimation-container">
                    <h2>Estimation</h2>
                    <div class="grid-container">
                        <div class="grid-item">
                            <label for="company">Solar Array</label>
                            <input type="text" id="solar_array" class="form-control" readonly ><!-- kw -->
                        </div>
                        <div class="grid-item">
                            <label for="company">Cost</label>
                            <input type="text" id="cost" class="form-control" readonly > <!-- &pound; --> 
                        </div>
                        <div class="grid-item">
                            <label for="company">Annual Electricity Bill</label>
                            <input type="text" class="form-control" readonly id="Estimated_Annual_Elec_Bill"> <!-- kw -->
                        </div>
                        <div class="grid-item">
                            <label for="company">Payback Period</label>
                            <input type="text" id="Payback_Period" class="form-control" readonly>
                        </div>
                        <div class="grid-item">
                            <label for="company">Annual Carbon Savings</label>
                            <input type="text" class="form-control" readonly id="Annual_Carbon_Savings">
                        </div>
                    </div>
                </div>
            </div>
            <div class="disclaimer">
                <h4>Disclaimer</h4>
                <ul>
                    <li>The calculation is indicative in nature. These are estimated guideline for initial consideration. Actuals may vary.</li>
                    <li>Optimum condition considered- South facing (30°-35° Angle).</li>
                    <li>Export Unit Cost = £0.07</li>
                </ul> 
            </div> 
         <!--    <div id="result_table" >
              <table style="width:100%;margin-left:100px;">
              <tr>
                <td id="Total_Annual_Savings">abc</td>
                <td id="Annual_Carbon_Savings"></td>
                <td id="Payback_Period"></td>
                <td id="Total_Annual_Savings"></td>

              </tr>
          </table>
            </div> -->
        <!-- </main> -->
    </div>
   
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.1/jquery.min.js"></script>
<script src="https://cdn.jsdelivr.net/jquery/1.12.4/jquery.min.js"></script>
<script src="https://cdn.jsdelivr.net/jquery.validation/1.15.1/jquery.validate.min.js"></script>
<script type="text/javascript">

/*function test(){
alert("hello");
}
*/

// Wait for the DOM to be ready
$(function() {
// Initialize form validation on the registration form.
 // It has the name attribute "registration"
 $("form[name='solarcalculatorform']").validate({
 // Specify validation rules
 rules: {
 name: "required",
      tel: "required",
      company: "required",
 email: {
 required: true,
 email: true
 }
 
 },
 // Specify validation error messages
 messages: {
 name: "Please enter your name",
 
 tel: "please enter your mobile number",
company: "please enter your company name",

 email: "Please enter a valid email address"
 },
 // Make sure the form is submitted to the destination defined
 // in the "action" attribute of the form when valid
 submitHandler: function(form) {
  $('#solar_calculatorform').submit(function(e){
      e.preventDefault();
  
  var name = $('#name').val();
  var email = $('#email').val();
  var company = $('#company').val();
  var tel = $('#tel').val();
  var annual_kw_hr = $('#annual_kw_hr').val();
  var electricity_rate = $('#electricity_rate').val();
  var data = {};
         $.ajax( {
          url: "{{ url('storeCalculation') }}",
          type:"POST",
          dataType: "json",
          data: {"_token": "{{ csrf_token() }}",
          'name':name,'email':email,'company':company,'tel':tel,'annual_kw_hr':annual_kw_hr,'electricity_rate':electricity_rate},
          success:function(result) {
            $('#result_table').show();
            window.location.hash = '#result_table';
           // (Math.round(num * 100) / 100).toFixed(2);
            $('#Payback_Period').html('');
            $('#solar_array').html('');
            $('#Annual_Carbon_Savings').html('');
            $('#Estimated_Annual_Elec_Bill').html('');
            $('#cost').html('');

            document.getElementById('Payback_Period').value = ((Math.round(result.Payback_Period * 100) / 100).toFixed(2)) + " Years";
            document.getElementById('solar_array').value = ((Math.round(result.solar_array * 100) / 100).toFixed(2)) + " KW";
            /*var payback_year = document.getElementById('Payback_Period').value;
           document.getElementById('Payback_Period').value = &pound; + cost*/;
            
            document.getElementById('Annual_Carbon_Savings').value = ((Math.round(result.Annual_Carbon_Savings * 100) / 100).toFixed(2)) + " Kg";
            document.getElementById('Estimated_Annual_Elec_Bill').value = ((Math.round(result.Estimated_Annual_Elec_Bill * 100) / 100).toFixed(2)) + " KW";
            document.getElementById('cost').value = "£ " + ((Math.round(result.cost * 100) / 100).toFixed(2));
            /*var cost = document.getElementById('cost').value;
                document.getElementById('cost').value = &pound; + cost;*/
              /*$('#Total_Annual_Savings').html('')
              $('#Annual_Carbon_Savings').append(result.Annual_Carbon_Savings);
              $('#Payback_Period').append(result.Payback_Period);
              $('#Savings_due_to_export').append(result.Savings_due_to_export);
              
            $('#Total_Annual_Savings').append(result.Total_Annual_Savings);*/
          },
          

      });
   
  });

  }
 });
});


/*$('#solar_calculatorform').submit(function(e){
      e.preventDefault();
  
  var name = $('#name').val();
  var email = $('#email').val();
  var company = $('#company').val();
  var tel = $('#tel').val();
  var annual_kw_hr = $('#annual_kw_hr').val();
  var electricity_rate = $('#electricity_rate').val();

         $.ajax( {
          url: "{{ url('storeCalculation') }}",
          type:"POST",
          dataType: "json",
          data: {"_token": "{{ csrf_token() }}",
          'name':name,'email':email,'company':company,'tel':tel,'annual_kw_hr':annual_kw_hr,'electricity_rate':electricity_rate},
          success:function(result) {
            
              $('#xyz').html('')
              $('#xyz').append(result.name);
              $('#abc').append("abc");
              $('#de').append("de");
              $('#result_table').show();
          },
          

      });
   
  });*/

</script>

  </body>
</html>

